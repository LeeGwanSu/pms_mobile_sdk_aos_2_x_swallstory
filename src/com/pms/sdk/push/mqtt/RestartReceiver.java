package com.pms.sdk.push.mqtt;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;

import java.net.URI;

/**
 * Private PUSH Restart Receiver
 * @author haewon
 *
 */
public class RestartReceiver extends BroadcastReceiver implements IPMSConsts
{
    @Override
    public synchronized void onReceive (final Context context, Intent intent)
    {
        if(intent != null)
        {
            String action = intent.getAction();
            MQTTScheduler scheduler = MQTTScheduler.getInstance();

            if(!TextUtils.isEmpty(action))
            {
                boolean lowVersion = true;
                int device = Build.VERSION.SDK_INT;
                Log.d("TAG", "Device build version: " + device + ", Release version: " + Build.VERSION.RELEASE);
                // Doze Mode 사용여부 체크
                if (device >= Build.VERSION_CODES.M && device >= 23)
                {
                    lowVersion = false;
                }

                if(ACTION_FORCE_START.equals(action))
                {
                    CLog.d("[ RestartReceiver ] action: ACTION_FORCE_START");
                    if (!lowVersion)
                    {
                        try
                        {
                            URI serverUri = new URI(PMSUtil.getMQTTServerUrl(context));

                            String serverProtocol = serverUri.getScheme();
                            String serverHost = serverUri.getHost();
                            int serverPort = serverUri.getPort();

                            String clientId = PMSUtil.getAppUserId(context);
                            if(!TextUtils.isEmpty(clientId))
                            {
                                int keepAlive = (int) MQTTScheduler.KEEP_ALIVE_TIME_HIGH / 1000;
                                MQTTBinder.newInstance(context).withInfo(clientId, serverProtocol, serverHost, serverPort, keepAlive).start(new MQTTBinder.IMQTTServiceCallback()
                                {
                                    // connection complete
                                    @Override
                                    public void onConnect(MQTTBinder.ConnectInfo child)
                                    {
                                        child.closeToAfterMillisecond(MQTTScheduler.KEEP_ALIVE_TIME_HIGH);
                                    }

                                    // connection close
                                    @Override
                                    public void onFinish()
                                    {
                                        CLog.d("[ Binder finish ]");
                                    }
                                });
                            }
                        }
                        catch (Exception e)
                        {
                            CLog.e("Receiver error(mqtt) " + e.getMessage());
                        }
                    }
                }
                else if (ACTION_START.equals(action) || ACTION_RESTART.equals(action) || ACTION_BOOT_COMPLETED.equals(action))
                {
                    if(FLAG_N.equals(PMSUtil.getPrivateFlag(context)) || FLAG_N.equals(PMSUtil.getMQTTFlag(context)))
                    {
                        return;
                    }

                    String msg = "";
                    if (ACTION_START.equals(action))
                    {
                        msg = "ACTION_START";
                    }
                    else if (ACTION_RESTART.equals(action))
                    {
                        msg = "ACTION_RESTART";
                    }
                    if (ACTION_BOOT_COMPLETED.equals(action))
                    {
                        msg = "ACTION_BOOT_COMPLETED";
                    }
                    CLog.d(msg);

                    if (lowVersion)
                    {
                        try
                        {
                            // alarm register
                            scheduler.setSchedule(context);

                            // mqtt service
                            Intent serviceIntent = new Intent(context, MQTTService.class);
                            serviceIntent.putExtra(MQTTService.INTENT_ACTION, action);
                            context.startService(serviceIntent);
                        }
                        catch (Exception e)
                        {
                            CLog.e("[ RestartReceiver ] service is not started! " + e.getMessage());
                        }
                    }
                }
                else
                {
                    CLog.w("[ RestartReceiver ] not support action: " + action);
                }
            }
            else
            {
                CLog.w("[ RestartReceiver ] intent action is empty");
            }
        }
        else
        {
            CLog.w("[ RestartReceiver ] intent is null");
        }
    }
}