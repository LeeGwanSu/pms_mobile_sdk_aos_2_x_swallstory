package com.pms.sdk.push.mqtt;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.DateUtil;

import java.util.Calendar;

public class MQTTScheduler
{
    private static MQTTScheduler instance;

    private static final int SCHEDULE_ALARM_UNIQUE_ID = 901232;

    /**
     * 배포 시, 유의점
     * DEFAULT_SCHEDULE_TIME : 10분 고정, 재연결 주기
     * KEEP_ALIVE_TIME_LOWLY : DEFAULT_SCHEDULE_TIME - 1분 = 9분, 6.0이하 단말기 강제종료 시간
     * KEEP_ALIVE_TIME_HIGH : 1분 고정, 6.0이상 단말기 강제종료 시간
     */
    public static final long DEFAULT_SCHEDULE_TIME = 1000 * 60 * 10;
    public static final long KEEP_ALIVE_TIME_LOWLY = DEFAULT_SCHEDULE_TIME - 1000 * 30 * 2;
    public static final long KEEP_ALIVE_TIME_HIGH = 1000 * 60 * 1;


    public AlarmManager privateAlarm;

    private MQTTScheduler() { }

    public static MQTTScheduler getInstance()
    {
        if(instance == null)
        {
            instance = new MQTTScheduler();
        }

        return instance;
    }

    public void setSchedule(Context ctx)
    {
        //AlarmManager
        long nextAlarmInMilliseconds = Calendar.getInstance().getTimeInMillis() + DEFAULT_SCHEDULE_TIME;
        CLog.d( "AlarmManager Schedule next alarm at " + DateUtil.getMillisecondsToDate(nextAlarmInMilliseconds));

        int ver = Build.VERSION.SDK_INT;
        CLog.i("Device os version: " + ver + ", Release version: " + Build.VERSION.RELEASE);
        try
        {
            cancelSchedule(ctx);

            if (ver >= Build.VERSION_CODES.M && ver >= 23)
            {
//                CLog.d( "Alarm schedule using setExactAndAllowWhileIdle.");
//                privateAlarm.setExactAndAllowWhileIdle(AlarmManager.RTC_WAKEUP, nextAlarmInMilliseconds, makePendingIntent(ctx));

                /*
                 * 코드 로직이 변동되어 동작할 일이 없으므로 알람등록 요청 시, 거절한다.
                 * 실시간 스트림 메세징 방식으로 전환
                 */
                Log.d("PMS", "This OS is not working.");

                return;
            }
            else if (ver >= Build.VERSION_CODES.KITKAT && ver >= 19)
            {
                CLog.d( "Alarm schedule using setExact.");
                privateAlarm.setExact(AlarmManager.RTC_WAKEUP, nextAlarmInMilliseconds, makePendingIntent(ctx));
            }
            else
            {
                CLog.d("Alarm schedule using set.");
                privateAlarm.set(AlarmManager.RTC_WAKEUP, nextAlarmInMilliseconds, makePendingIntent(ctx));
            }
        }
        catch (Exception e)
        {
            CLog.e(e.getMessage());
        }

    }

    private void cancelSchedule(Context ctx)
    {
        CLog.d("cancelSchedule init..");
        if (privateAlarm == null)
        {
            privateAlarm = (AlarmManager) ctx.getSystemService(Service.ALARM_SERVICE);
        }

        try
        {
            Intent intent = new Intent(ctx, RestartReceiver.class);
            intent.setAction(IPMSConsts.ACTION_RESTART);

            PendingIntent registerAlarm = PendingIntent.getBroadcast(ctx, SCHEDULE_ALARM_UNIQUE_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);

            privateAlarm.cancel(registerAlarm);
            registerAlarm.cancel();

        }
        catch (Exception e)
        {
            CLog.e("[ cancelSchedule ] exception: " + e.getMessage());
        }
    }

    private PendingIntent makePendingIntent (Context context)
    {
        Intent intent = new Intent(context, RestartReceiver.class);
        intent.setAction(IPMSConsts.ACTION_RESTART);

        return PendingIntent.getBroadcast(context, SCHEDULE_ALARM_UNIQUE_ID, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }
}
