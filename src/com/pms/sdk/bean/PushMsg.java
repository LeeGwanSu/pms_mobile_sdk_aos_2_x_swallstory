package com.pms.sdk.bean;

import android.content.Intent;
import android.os.Bundle;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.push.mqtt.MQTTService;
import org.json.JSONObject;

public class PushMsg implements IPMSConsts {
    public static final int EMPTY_TYPE_MSGID = 1;
    public static final int EMPTY_TYPE_NOTITITLE = 2;
    public static final int EMPTY_TYPE_NOTIMSG = 3;
    public static final int EMPTY_TYPE_MSGTYPE = 4;
    public static final int DEFAULT_GCM_PASER = 0;
    public static final int PRIVATE_PASER = 1;
    public final Intent intent;
    public String msgId;
    public String notiTitle;
    public String notiMsg;
    public String notiImg;
    public String message;
    public String sound;
    public String msgType;
    public String data;

    public PushMsg(Bundle extras) {
        msgId = extras.getString(KEY_MSG_ID);
        if (msgId == null || msgId.isEmpty()) {
            final String msgIdV2 = extras.getString(KEY_MSG_ID_V2);
            if (msgIdV2 != null) {
                msgId = msgIdV2;
                extras.putString(KEY_MSG_ID, msgIdV2);
            }
        }

        notiTitle = extras.getString(KEY_NOTI_TITLE);
        notiMsg = extras.getString(KEY_NOTI_MSG);
        notiImg = extras.getString(KEY_NOTI_IMG);
        message = extras.getString(KEY_MSG);
        sound = extras.getString(KEY_SOUND);

        msgType = extras.getString(KEY_MSG_TYPE);
        if (msgType == null || msgType.isEmpty()) {
            final String msgTypeV2 = extras.getString(KEY_MSG_TYPE_V2);
            if (msgTypeV2 != null) {
                msgType = msgTypeV2;
                extras.putString(KEY_MSG_TYPE, msgTypeV2);
            }
        }

        data = extras.getString(KEY_DATA);
        this.intent = null;
    }

    public PushMsg(Intent intent, String data) {
        CLog.i("onReceive:receive from private server");
        // set push info

        try {
            JSONObject msgObj = new JSONObject(data);

            if (msgObj.has(KEY_MSG_ID)) {
                msgId = msgObj.getString(KEY_MSG_ID);
                intent.putExtra(KEY_MSG_ID, msgObj.getString(KEY_MSG_ID));
            }

            if (msgObj.has(KEY_MSG_ID_V2)) {
                if (msgId == null || msgId.isEmpty()) {
                    msgId = msgObj.getString(KEY_MSG_ID_V2);
                    intent.putExtra(KEY_MSG_ID, msgObj.getString(KEY_MSG_ID_V2));
                }
                intent.putExtra(KEY_MSG_ID_V2, msgObj.getString(KEY_MSG_ID_V2));
            }

            if (msgObj.has(KEY_NOTI_TITLE)) {
                intent.putExtra(KEY_NOTI_TITLE, msgObj.getString(KEY_NOTI_TITLE));
            }

            if (msgObj.has(KEY_MSG_TYPE)) {
                msgType = msgObj.getString(KEY_MSG_TYPE);
                intent.putExtra(KEY_MSG_TYPE, msgObj.getString(KEY_MSG_TYPE));
            }

            if (msgObj.has(KEY_MSG_TYPE_V2)) {
                if (msgType == null || msgType.isEmpty()) {
                    msgType = msgObj.getString(KEY_MSG_TYPE_V2);
                    intent.putExtra(KEY_MSG_TYPE, msgObj.getString(KEY_MSG_TYPE_V2));
                }
                intent.putExtra(KEY_MSG_TYPE_V2, msgObj.getString(KEY_MSG_TYPE_V2));
            }
            if (msgObj.has(KEY_NOTI_MSG)) {
                intent.putExtra(KEY_NOTI_MSG, msgObj.getString(KEY_NOTI_MSG));
            }
            if (msgObj.has(KEY_MSG)) {
                intent.putExtra(KEY_MSG, msgObj.getString(KEY_MSG));
            }
            if (msgObj.has(KEY_SOUND)) {
                intent.putExtra(KEY_SOUND, msgObj.getString(KEY_SOUND));
            }
            if (msgObj.has(KEY_NOTI_IMG)) {
                intent.putExtra(KEY_NOTI_IMG, msgObj.getString(KEY_NOTI_IMG));
            }
            if (msgObj.has(KEY_DATA)) {
                intent.putExtra(KEY_DATA, msgObj.getString(KEY_DATA));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.intent = intent;
    }

    @Override
    public String toString() {
        return String.format("onMessage:msgId=%s, notiTitle=%s, notiMsg=%s, notiImg=%s, message=%s, sound=%s, msgType=%s, data=%s", msgId, notiTitle,
                notiMsg, notiImg, message, sound, msgType, data);
    }

    public int getEmptyType() {

        if (StringUtil.isEmpty(msgId)) {
            return EMPTY_TYPE_MSGID;
        }

        if (StringUtil.isEmpty(notiTitle)) {
            return EMPTY_TYPE_NOTITITLE;
        }

        if (StringUtil.isEmpty(notiMsg)) {
            return EMPTY_TYPE_NOTIMSG;
        }

        if (StringUtil.isEmpty(msgType)) {
            return EMPTY_TYPE_MSGTYPE;
        }

        return -1;
    }
}
