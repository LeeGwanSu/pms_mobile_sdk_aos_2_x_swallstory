package com.pms.sdk;

import java.io.Serializable;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.TextView;

import com.pms.sdk.bean.Msg;
import com.pms.sdk.bean.MsgGrp;
import com.pms.sdk.common.util.CLog;
import com.pms.sdk.common.util.PMSUtil;
import com.pms.sdk.common.util.PhoneState;
import com.pms.sdk.common.util.Prefs;
import com.pms.sdk.common.util.StringUtil;
import com.pms.sdk.db.PMSDB;
import com.pms.sdk.push.PushReceiver;
import com.pms.sdk.push.mqtt.MQTTService;
import com.pms.sdk.push.mqtt.RestartReceiver;
import com.pms.sdk.view.Badge;

/**
 * @author erzisk
 * @version 201803021709
 * @description pms (solution main class)
 * [2014.01.13 09:31] MQTT Client 안정화 & Connect Check 부분 수정함. <br>
 * [2014.02.07 17:12] Collect 부분 수정함. <br>
 * [2014.02.11 10:18] CollectLog getParam 부분 수정함. <br>
 * [2014.02.21 17:23] MQTT Client 안정화 및 Prvate 서버 Protocol 적용 가능하게 수정함.<br>
 * [2014.02.25 10:12] 안쓰는 method 삭제함.<br>
 * [2014.03.03 10:36] 팝업 셋팅 default 설정 가능하게 변경.<br>
 * [2014.03.05 15:31] MQTT Client 관련 Service 추가.<br>
 * [2014.03.05 20:37] MQTT Client STOP 재시작하는 현상 수정.<br>
 * [2014.03.13 10:47] MQTT Client keepAlive 수정함. sleep mode 삭제.<br>
 * [2014.03.28 09:29] DisusePms.m API 추가함.<br>
 * [2014.05.14 20:06] DisusePms.m API 삭제 & Popup 설정값 추가.<br>
 * [2014.06.02 20:39] Popup WebView background 수정함. <br>
 * [2014.06.05 18:24] 다른앱 실행시 팝업창 미 노출. <br>
 * [2014.06.09 11:05] 팝업창에 대한 Flag 값 추가함. <br>
 * [2014.06.11 17:32] 텍스트 푸쉬일때 toast 메세지로 전환할수 있도록 수정. <br>
 * [2014.06.16 20:44] 다른앱 사용시 안뜨는 플래그 추가함. <br>
 * [2014.06.24 09:10] ReadMSg UserMsgId로 가능하게 수정함. <br>
 * [2014.07.02 15:28] 연속 발송시 1건씩 빠지는 문제 수정함. <br>
 * [2014.07.03 12:28] MQTT Wake Lock 쪽 예외처리 추가함. <br>
 * [2014.07.15 15:20] MQTT ping 관련 소스 추가함. 필요없는 소스 삭제. <br>
 * [2014.07.24 10:41] MQTT 버그 수정 & pushImg 저장 가능하게 수정함.<br>
 * [2014.07.25 10:30] 자동 ReadMsg 104 오류발생시 JSON 재구성해서 다시 호출하게 수정함.<br>
 * [2014.08.01 15:01] Collect Log 버그 수정함. Push 받을 떄 null 체크하는 부분 계선.<br>
 * [2014.08.04 13:29] MQTT 관련해서 category 사용자가 정의하게 수정함.<br>
 * [2014.08.07 13:26] MQTT 안정화 & API Error Flag 수정. <br>
 * [2014.08.21 10:41] MQTT 안정화 & API 호출시 Cache Clear 루틴 추가함.<br>
 * [2014.09.01 13:00] Vollry lib 업데이트. <br>
 * [2014.09.11 10:49] Notification Icon을 블러오는 위치를 Androidmanifest.xml 쪽으로 바꿈. <br>
 * [2014.09.17 10:15] DB쪽 버그사항 수정함. <br>
 * [2014.10.15 10:00] 필요없는 Method 삭제. <br>
 * [2014.10.20 20:57] Android OS 5.0에서 상태창 이미지 표시 안되는 버그 수정함. <br>
 * [2014.11.05 17:21] Notification Priority Level 2로 조정함. <br>
 * [2014.11.17 13:25] Cust_Id 기존 저장값에서 메모리로 저장되게 수정함. <br>
 * [2015.03.13 16:42] 구글 정책에 따른 앱 실행중일떄만 팝업창 노출. <br>
 * [2016.02.02 16:42] 다른앱 사용시 체크 안되는 문제 수정함. <br>
 * [2016.04.29 09:42] X509TrustManager 코드 삭제하고 대응 코드 삽입함.<br>
 * [2016.11.29 09:45] doze mode broadcast 패치1 적용 <br>
 * [2016.12.14 13:21] Icon에 대한 버그 사항 수정함.<br>
 * [2017.10.27 17:50] * Queue Manager 추가 Single tone 으로 관리 (Image Loader 및 APIManager)
 * * DB 중복 Open으로 인해 강제종료 문제 수정
 * * PushPopup 발생 시 강제 종료 문제 수정
 * * UUID 생성 로직 변경, Device정보를 이용하여 UUID를 작성하는 경로 이전에 Device 정보 없이 발급하는 방법으로
 * UUID 발급, 발급 실패 시 Device정보를 필요로 하는 UUID 발급 방법으로 발급 시도함
 * [2018.01.19 10:17] * JobService 적용 (8.0 대응)
 * [2018.02.27 17:26] * Release(실수로 201710271750으로 나간 상태)
 * FCM 적용, 8.0 대응 명시적 호출 및 타겟 버전에 따라 Push받지 못하는 문제 수정
 * [2018.02.28 18:12] * Release onMessageReceived 시 ID 비교 하지 않고 Notify하도록 수정
 * [2018.03.02 17:09] * Release Payload parser수정 (msgId==i, msgType==t)
 * [2018.09.20 15:39] Android 8.0 대응, 이미지 캐시 대응, WRITE_STORAGE_PERMISSION 제거, 잠금화면체크, 벨소리 볼륨 대응
 * [2019.01.23 10:54] 벨소리 볼륨 부분 변경, FCMInstanceId Deprecated 대응
 * [2019.01.23 15:34] 업체가 FCM 적용할 시간 없대서 GCM 해줌
 * [2019.01.24 13:56] GCM 푸시 왔을때 노티 띄우도록 수정
 * [2019.01.28 09:32] GCMInstanceId 추가
 * [2019.01.28 12:47] NotificationCompat 쓰지 않도록 함
 * [2019.03.29 15:20] FCM 적용
 * [2019.05.09 11:13] Android 8.0 이상부터 이미지 푸시 안되는거 수정, 벨소리 볼륨 로직 제거, MQTT 안쓰는 클래스 제거, FCM 토큰 발급 개선
 * [2020.07.09 15:21] 고객사 요청으로 푸시 알림 시간 표시 기능 추가, ReadMsg kk->HH 변경, 채널 재생성 로직 제거
 */
public class PMS implements IPMSConsts, Serializable {

    private static final long serialVersionUID = 1L;
    public static String mCust_id = "";
    private static PMS instancePms;
    private static PMSPopup instancePmsPopup;
    private final PMSDB mDB;
    private final Prefs mPrefs;
    private Context mContext;
    private OnReceivePushListener onReceivePushListener;

    /**
     * constructor
     *
     * @param context
     */
    private PMS(Context context) {
        this.mDB = PMSDB.getInstance(context);
        this.mPrefs = new Prefs(context);
        CLog.i("Version:" + PMS_VERSION + ",UpdateDate:"+PMS_VERSION_UPDATE_DATE);

        initOption(context);
    }

    /**
     * getInstance
     *
     * @param context
     * @param projectId
     * @return
     */
    public static PMS getInstance(Context context, String projectId) {
        PMSUtil.setGCMProjectId(context, projectId);
        return getInstance(context);
    }

    /**
     * getInstance
     *
     * @param context
     * @return
     */
    public static PMS getInstance(Context context) {
        CLog.d("getInstance:projectId=" + PMSUtil.getGCMProjectId(context));
        if (instancePms == null) {
            instancePms = new PMS(context);
//            if (PhoneState.isAvailablePush()) {
//                PushReceiver.gcmRegister(context, PMSUtil.getGCMProjectId(context));
//            }
        }
        instancePms.setmContext(context);
        return instancePms;
    }

    public static PMSPopup getPopUpInstance() {
        return instancePmsPopup;
    }

    /**
     * clear
     *
     * @return
     */
    public static boolean clear() {
        try {
            PMSUtil.setDeviceCertStatus(instancePms.mContext, DEVICECERT_PENDING);
            instancePms.unregisterReceiver();
            instancePms = null;
            mCust_id = "";
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    /**
     * initialize option
     */
    private void initOption(Context context) {
        if (StringUtil.isEmpty(mPrefs.getString(PREF_NOTI_FLAG))) {
            mPrefs.putString(PREF_NOTI_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_MSG_FLAG))) {
            mPrefs.putString(PREF_MSG_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_RING_FLAG))) {
            mPrefs.putString(PREF_RING_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_VIBE_FLAG))) {
            mPrefs.putString(PREF_VIBE_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_ALERT_FLAG))) {
            mPrefs.putString(PREF_ALERT_FLAG, "Y");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_MAX_USER_MSG_ID))) {
            mPrefs.putString(PREF_MAX_USER_MSG_ID, "-1");
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_SCREEN_WAKEUP_FLAG))) {
            mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, "Y");
        }
        if (StringUtil.isEmpty(PMSUtil.getServerUrl(context))) {
            PMSUtil.setServerUrl(context, API_SERVER_URL);
        }
        if (StringUtil.isEmpty(mPrefs.getString(PREF_PUSH_POPUP_ACTIVITY))) {
            mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, DEFAULT_PUSH_POPUP_ACTIVITY);
        }
        if (mPrefs.getInt(PREF_PUSH_POPUP_SHOWING_TIME) < 1) {
            mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, 10000);
        }
    }

    /**
     * start mqtt service
     */
    public void startMQTTService(Context context) {
        if (FLAG_Y.equals(PMSUtil.getMQTTFlag(context))) {
            context.sendBroadcast(new Intent(context, RestartReceiver.class).setAction(ACTION_START));
        } else {
            context.stopService(new Intent(context, MQTTService.class));
        }
    }

    public void stopMQTTService(Context context) {
        context.stopService(new Intent(context, MQTTService.class));
    }

    private void setmContext(Context context) {
        this.mContext = context;
    }

    /**
     * push token 세팅
     * @param pushToken
     */
    public void setPushToken(String pushToken) {
        PMSUtil.setGCMToken(mContext, pushToken);
    }

    private void unregisterReceiver() {
        Badge.getInstance(mContext).unregisterReceiver();
    }

    public void setPopupSetting(Boolean state, String title) {
        instancePmsPopup = PMSPopup.getInstance(mContext, mContext.getPackageName(), state, title);
    }

    /**
     * get cust id
     *
     * @return
     */
    public String getCustId() {
        CLog.i("getCustId");
        return mCust_id;
    }

    /**
     * cust id 세팅
     *
     * @param custId
     */
    public void setCustId(String custId) {
        CLog.i("setCustId");
        CLog.d("setCustId:custId=" + custId);
        if (custId.equals(mCust_id) == false) {
            CLog.i("DeviceCert:new user");
            mDB.deleteAll();
        }
        mCust_id = custId;
    }

    public void setIsPopupActivity(Boolean ispopup) {
        PMSUtil.setPopupActivity(mContext, ispopup);
    }

    public void setNotiOrPopup(Boolean isnotiorpopup) {
        PMSUtil.setNotiOrPopup(mContext, isnotiorpopup);
    }

    /**
     * set inbox activity
     *
     * @param inboxActivity
     */
    public void setInboxActivity(String inboxActivity) {
        mPrefs.putString(PREF_INBOX_ACTIVITY, inboxActivity);
    }

    /**
     * set push popup activity
     *
     * @param classPath
     */
    public void setPushPopupActivity(String classPath) {
        mPrefs.putString(PREF_PUSH_POPUP_ACTIVITY, classPath);
    }

    /**
     * set pushPopup showing time
     *
     * @param pushPopupShowingTime
     */
    public void setPushPopupShowingTime(int pushPopupShowingTime) {
        mPrefs.putInt(PREF_PUSH_POPUP_SHOWING_TIME, pushPopupShowingTime);
    }

    /**
     * get msg flag
     *
     * @return
     */
    public String getMsgFlag() {
        return mPrefs.getString(PREF_MSG_FLAG);
    }

    /**
     * get noti flag
     *
     * @return
     */
    public String getNotiFlag() {
        return mPrefs.getString(PREF_NOTI_FLAG);
    }

    /**
     * set large noti icon
     *
     * @param resId
     */
    public void setLargeNotiIcon(int resId) {
        mPrefs.putInt(PREF_LARGE_NOTI_ICON, resId);
    }

    /**
     * set noti cound
     *
     * @param resId
     */
    public void setNotiSound(int resId) {
        mPrefs.putInt(PREF_NOTI_SOUND, resId);
    }

    /**
     * set noti receiver
     *
     * @param intentAction
     */
    public void setNotiReceiver(String intentAction) {
        mPrefs.putString(PREF_NOTI_RECEIVER, intentAction);
    }
    public void setPushReceiver(String intentAction) {
        mPrefs.putString(IPMSConsts.PREF_PUSH_RECEIVER_ACTION, intentAction);
    }

    /**
     * set noti receiver class
     *
     * @param intentClass
     */
    public void setNotiReceiverClass(String intentClass) {
        mPrefs.putString(PREF_NOTI_RECEIVER_CLASS, intentClass);
    }
    public void setPushReceiverClass(String intentClass) {
        mPrefs.putString(IPMSConsts.PREF_PUSH_RECEIVER_CLASS, intentClass);
    }

    /**
     * set ring mode
     *
     * @param isRingMode
     */
    public void setRingMode(boolean isRingMode) {
        mPrefs.putString(PREF_RING_FLAG, isRingMode ? "Y" : "N");
    }

    /**
     * set vibe mode
     *
     * @param isVibeMode
     */
    public void setVibeMode(boolean isVibeMode) {
        mPrefs.putString(PREF_VIBE_FLAG, isVibeMode ? "Y" : "N");
    }

    /**
     * set popup noti
     *
     * @param isShowPopup
     */
    public void setPopupNoti(boolean isShowPopup) {
        mPrefs.putString(PREF_ALERT_FLAG, isShowPopup ? "Y" : "N");
    }

    /**
     * set screen wakeup
     *
     * @param isScreenWakeup
     */
    public void setScreenWakeup(boolean isScreenWakeup) {
        mPrefs.putString(PREF_SCREEN_WAKEUP_FLAG, isScreenWakeup ? "Y" : "N");
    }

    public OnReceivePushListener getOnReceivePushListener() {
        return onReceivePushListener;
    }

    public void setOnReceivePushListener(OnReceivePushListener onReceivePushListener) {
        this.onReceivePushListener = onReceivePushListener;
    }

    /**
     * set debugTag
     *
     * @param tagName
     */
    public void setDebugTAG(String tagName) {
        CLog.setTagName(tagName);
    }

    /**
     * set debug mode
     *
     * @param debugMode
     */
    public void setDebugMode(boolean debugMode) {
        CLog.setDebugMode(mContext, debugMode);
    }

    public void bindBadge(Context c, int id) {
        Badge.getInstance(c).addBadge(c, id);
    }

    public void bindBadge(TextView badge) {
        Badge.getInstance(badge.getContext()).addBadge(badge);
    }

    /**
     * show Message Box
     *
     * @param c
     */
    public void showMsgBox(Context c) {
        showMsgBox(c, null);
    }

    public void showMsgBox(Context c, Bundle extras) {
        CLog.i("showMsgBox");
        if (PhoneState.isAvailablePush()) {
            try {
                Class<?> inboxActivity = Class.forName(mPrefs.getString(PREF_INBOX_ACTIVITY));

                Intent i = new Intent(mContext, inboxActivity);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                if (extras != null) {
                    i.putExtras(extras);
                }
                c.startActivity(i);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }

            // Intent i = new Intent(INBOX_ACTIVITY);
            // i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);
            // if (extras != null) {
            // i.putExtras(extras);
            // }
            // c.startActivity(i);
        }
    }

    public void closeMsgBox(Context c) {
        Intent i = new Intent(c, Badge.class).setAction(RECEIVER_CLOSE_INBOX);
        c.sendBroadcast(i);
    }

    /*
     * ===================================================== [start] database =====================================================
     */

    /**
     * select MsgGrp list
     *
     * @return
     */
    public Cursor selectMsgGrpList() {
        return mDB.selectMsgGrpList();
    }

    /**
     * select MsgGrp
     *
     * @param msgCode
     * @return
     */
    public MsgGrp selectMsGrp(String msgCode) {
        return mDB.selectMsgGrp(msgCode);
    }

    /**
     * select new msg Cnt
     *
     * @return
     */
    public int selectNewMsgCnt() {
        return mDB.selectNewMsgCnt();
    }

    /**
     * select Msg List
     *
     * @param page
     * @param row
     * @return
     */
    public Cursor selectMsgList(int page, int row) {
        return mDB.selectMsgList(page, row);
    }

    /**
     * select Msg List
     *
     * @param msgCode
     * @return
     */
    public Cursor selectMsgList(String msgCode) {
        return mDB.selectMsgList(msgCode);
    }

    /**
     * select Msg
     *
     * @param msgId
     * @return
     */
    public Msg selectMsgWhereMsgId(String msgId) {
        return mDB.selectMsgWhereMsgId(msgId);
    }

    /**
     * select Msg
     *
     * @param userMsgID
     * @return
     */
    public Msg selectMsgWhereUserMsgId(String userMsgID) {
        return mDB.selectMsgWhereUserMsgId(userMsgID);
    }

    /**
     * select query
     *
     * @param sql
     * @return
     */
    public Cursor selectQuery(String sql) {
        return mDB.selectQuery(sql);
    }

    /**
     * update MsgGrp
     *
     * @param msgCode
     * @param values
     * @return
     */
    public long updateMsgGrp(String msgCode, ContentValues values) {
        return mDB.updateMsgGrp(msgCode, values);
    }

    /**
     * update read msg
     *
     * @param msgGrpCd
     * @param firstUserMsgId
     * @param lastUserMsgId
     * @return
     */
    public long updateReadMsg(String msgGrpCd, String firstUserMsgId, String lastUserMsgId) {
        return mDB.updateReadMsg(msgGrpCd, firstUserMsgId, lastUserMsgId);
    }

    /**
     * update read msg where userMsgId
     *
     * @param userMsgId
     * @return
     */
    public long updateReadMsgWhereUserMsgId(String userMsgId) {
        return mDB.updateReadMsgWhereUserMsgId(userMsgId);
    }

    /**
     * update read msg where msgId
     *
     * @param msgId
     * @return
     */
    public long updateReadMsgWhereMsgId(String msgId) {
        return mDB.updateReadMsgWhereMsgId(msgId);
    }

    /**
     * delete Msg
     *
     * @param userMsgId
     * @return
     */
    public long deleteUserMsgId(String userMsgId) {
        return mDB.deleteUserMsgId(userMsgId);
    }

    /**
     * delete Msg
     *
     * @param MsgId
     * @return
     */
    public long deleteMsgId(String MsgId) {
        return mDB.deleteMsgId(MsgId);
    }

    /**
     * delete MsgGrp
     *
     * @param msgCode
     * @return
     */
    public long deleteMsgGrp(String msgCode) {
        return mDB.deleteMsgGrp(msgCode);
    }

    /**
     * delete expire Msg
     *
     * @return
     */
    public long deleteExpireMsg() {
        return mDB.deleteExpireMsg();
    }

    /**
     * delete empty MsgGrp
     *
     * @return
     */
    public void deleteEmptyMsgGrp() {
        mDB.deleteEmptyMsgGrp();
    }

    /**
     * delete all
     */
    public void deleteAll() {
        mDB.deleteAll();
    }

    /*
     * ===================================================== [end] database =====================================================
     */
    public void setShowNotificationWhen(boolean isShow)
    {
        mPrefs.putString(PREF_SHOW_NOTIFICATION_WHEN, isShow ? "Y" : "N");
    }

    public void setNotificationClickActivityClass(String action, Class activity) {
        PMSUtil.setActivityToMoveWhenClick(mContext, action, activity, false);
    }

    public void setNotificationClickActivityUseBackStack(String action, Class activity) {
        PMSUtil.setActivityToMoveWhenClick(mContext, action, activity, true);
    }

	public void createNotificationChannel(){
		PMSUtil.createNotificationChannel(mContext);
	}
}
