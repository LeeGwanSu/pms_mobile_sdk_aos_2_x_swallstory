package com.samsungwelstory.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;

import com.pms.sdk.IPMSConsts;
import com.pms.sdk.PMS;
import com.pms.sdk.common.util.Prefs;
import com.samsungwelstory.mobilewinkwelcom.activity.SplashActivity;
import com.samsungwelstory.common.IPMSDEMOConts;

public class BaseFragment extends Fragment implements IPMSConsts, IPMSDEMOConts {

	protected Context mCon = null; // Activity Context

	protected PMS mPms = null; // PMS Instance

	protected Prefs mPref = null; // SharedPreferences Instance

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onCreate(android.os.Bundle)
	 */
	@Override
	public void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.mCon = getActivity();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater, android.view.ViewGroup, android.os.Bundle)
	 */
	@Override
	public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return super.onCreateView(inflater, container, savedInstanceState);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.support.v4.app.Fragment#onActivityCreated(android.os.Bundle)
	 */
	@Override
	public void onActivityCreated (Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		this.mPms = PMS.getInstance(mCon);
		this.mPref = new Prefs(mCon);
	}

	/**
	 * Page 이동에 관련된 Method
	 * 
	 * @param id
	 *        View ID
	 * @param args
	 *        각 페이지에 넘길 ID값
	 */
	protected void setFragment (int id, Object... args) {
		((SplashActivity) getActivity()).setFragment(id, args);
	}

	/**
	 * Fragment Back Key 제어
	 */
	@SuppressLint("NewApi")
	public void onBackPressed () {
		((SplashActivity) getActivity()).onKeyDown(KeyEvent.KEYCODE_BACK, null);
	}
}
